package com.sarcasm;

import com.amazonaws.AmazonClientException;
import com.amazonaws.regions.Region;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.*;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.DeleteQueueRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

public class HandleWorkersMsg implements Runnable {

    //fields
    private final static String MANAGER_WORKERS_SQS_NAME = "manager-workers";
    private final static String WORKERS_MANAGER_SQS_NAME = "workers-manager";
    private static String MANAGER_WORKERS_SQS_URL = null;
    private static String WORKERS_MANAGER_SQS_URL = null;
    private static String workersManagerSqsUrl;
    private ConcurrentHashMap<String, Integer> numOfReviewsPerFile;
    private ConcurrentHashMap<String, String> filesUrls;
    private static AtomicBoolean terminate;
    private static String bucketName;

    //vars
    private static AmazonSQS SQS = AmazonSQSClientBuilder.standard()
            .withRegion(managerApp.REGION)
            .build();
    private static AmazonEC2 EC2 = AmazonEC2ClientBuilder.standard()
            .withRegion(managerApp.REGION)
            .build();
    private static AmazonS3 S3 = AmazonS3ClientBuilder.standard()
            .withRegion(managerApp.REGION)
            .build();

    //Constractor
    public HandleWorkersMsg(String workersManagerSqsUrl, ReviewsMap reviewsMap) {
        this.workersManagerSqsUrl = workersManagerSqsUrl;
        this.numOfReviewsPerFile = reviewsMap.getNumOfReviewsPerFile();
        this.filesUrls = reviewsMap.getFilesUrls();
        this.terminate = reviewsMap.terminate;
    }

    public void run() {
        //forever download a msg from the queue
        while (true) {
            System.out.println("waiting for massage from workers");
            ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(workersManagerSqsUrl);
            List<Message> messages = SQS.receiveMessage(receiveMessageRequest).getMessages();
            for (Message message : messages) {
                if (MANAGER_WORKERS_SQS_URL == null || WORKERS_MANAGER_SQS_URL == null){
                    MANAGER_WORKERS_SQS_URL = getSqsUrl(MANAGER_WORKERS_SQS_NAME);
                    WORKERS_MANAGER_SQS_URL = getSqsUrl(WORKERS_MANAGER_SQS_NAME);
                }
                ObjectMapper mapper = new ObjectMapper();
                ReviewResult reviewResult = null;
                try {
                    reviewResult = mapper.readValue(message.getBody(), ReviewResult.class);
                    System.out.println("recived message id: " + reviewResult.getReview().getId() +
                            "from file: " + reviewResult.getReview().getFileName());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String fileName = reviewResult.getReview().getFileName();
                int valueOfEntry;
                try {
                    valueOfEntry =  numOfReviewsPerFile.get(fileName);
                }
                catch (NullPointerException e){
                    //in case we got duplicate message and the file already deleted
                    valueOfEntry = 0;
                }
                System.out.println("review left in file: "+ fileName + "are: " + valueOfEntry);
                valueOfEntry--;
                if (valueOfEntry != 0) {
                    BufferedWriter bufferedWriter;
                    try {
                        bufferedWriter = new BufferedWriter(new FileWriter(fileName, true));
                        bufferedWriter.write(message.getBody() + "\n");
                        bufferedWriter.close();
                        //System.out.println("write result to file " + fileName);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    numOfReviewsPerFile.put(fileName, valueOfEntry);
                    //delete msg from queue
                    SQS.deleteMessage(WORKERS_MANAGER_SQS_URL, message.getReceiptHandle());
                    //System.out.println("delete message from workers-manager queue");
                } else {
                    if(valueOfEntry == 0) {
                        System.out.println("done" + fileName);
                        BufferedWriter bufferedWriter;
                        try {
                            bufferedWriter = new BufferedWriter(new FileWriter(fileName, true));
                            bufferedWriter.write(message.getBody());
                            bufferedWriter.close();
                            //System.out.println("write last review to file " + fileName);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        bucketName = reviewResult.getReview().getBucketName();
                        UploadFileToS3(fileName, reviewResult.getReview().getOutputKeyName());
                        System.out.println("uploaded the result file to s3 " + bucketName);
                        //send a msgToApp to app regarding the location of the file
                        MsgToApp msgToApp = new MsgToApp();
                        msgToApp.setBucketName(reviewResult.getReview().getBucketName());
                        msgToApp.setFileName(fileName);
                        msgToApp.setOutputKeyName(reviewResult.getReview().getOutputKeyName());
                        try {
                            addMsgToSqs(mapper.writeValueAsString(msgToApp), filesUrls.get(fileName));
                            System.out.println("added message to queue manager-app");
                        } catch (JsonProcessingException e) {
                            e.printStackTrace();
                        }
                        //remove from maps
                        numOfReviewsPerFile.remove(fileName);
                        filesUrls.remove(fileName);
                    }
                    //delete msgToApp from queue
                    SQS.deleteMessage(WORKERS_MANAGER_SQS_URL, message.getReceiptHandle());
                    System.out.println("delete message from workers-manager queue");
                }
            }
            if (terminate.get() && (numOfReviewsPerFile.isEmpty())) {
                handleTerminateMode();
                return;
            }
        }
    }

    private void handleTerminateMode() {
        //delete workers
        System.out.println("handel terminate message");
        DescribeInstancesRequest request = new DescribeInstancesRequest();
        List<String> tagValue = new ArrayList<>();
        tagValue.add("worker");
        DescribeInstancesResult response = EC2.describeInstances(request.withFilters(new Filter().withName("tag:serverType").withValues(tagValue)));
        for (Reservation reservation : response.getReservations()) {
            for (Instance instance : reservation.getInstances()) {
                EC2.terminateInstances(new TerminateInstancesRequest()
                        .withInstanceIds(instance.getInstanceId()));
                //System.out.println("terminate worker");
            }
        }
        //delete SQS queues
        SQS.deleteQueue(new DeleteQueueRequest(MANAGER_WORKERS_SQS_URL));
        SQS.deleteQueue(new DeleteQueueRequest(WORKERS_MANAGER_SQS_URL));
        System.out.println("delete worker queues");
    }


    private static String getSqsUrl(String sqsName) {
        try {
            String result = SQS.getQueueUrl(sqsName).getQueueUrl();
            System.out.println("get sqs: "+ sqsName + "url -------------------------------" +result);
            return result;
        }catch (com.amazonaws.services.sqs.model.QueueDoesNotExistException e) {
            System.out.println("queue was not found");
            return null;
        }
    }

    private static void UploadFileToS3(String inputFile, String path) {
        try {
            System.out.println("Uploading a new object to S3 from a file\n");
            File file = new File(inputFile);
            S3.putObject(new PutObjectRequest(
                    bucketName, path + file.getName(), file));
            file.delete();
        } catch (AmazonClientException e) {
            e.printStackTrace();
        }
    }

    private static void addMsgToSqs(String massage, String url) {
        System.out.println("add masage to manager app queue " + url);
        SQS.sendMessage(new SendMessageRequest()
                .withQueueUrl(url)
                .withMessageBody(massage));
    }
}
