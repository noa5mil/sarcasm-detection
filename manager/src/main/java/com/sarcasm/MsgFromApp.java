package com.sarcasm;

public class MsgFromApp {

    private String fileName;
    private String bucketName;
    private String inputKeyName;
    private String outputKeyName;
    private String appSqsUrl;
    private boolean isTerminate;
    private int n;


    public boolean isTerminate() {
        return isTerminate;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public void setTerminate(boolean terminate) {
        isTerminate = terminate;
    }

    public String getAppSqsUrl() {
        return appSqsUrl;
    }

    public void setAppSqsUrl(String appSqsUrl) {
        this.appSqsUrl = appSqsUrl;
    }

    public String getFileName() {
        return this.fileName;
    }

    public String getBucketName() {
        return this.bucketName;
    }

    public String getInputKeyName() {
        return this.inputKeyName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public void setInputKeyName(String inputKeyName) {
        this.inputKeyName = inputKeyName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getOutputKeyName() {
        return outputKeyName;
    }

    public void setOutputKeyName(String outputKeyName) {
        this.outputKeyName = outputKeyName;
    }
}

