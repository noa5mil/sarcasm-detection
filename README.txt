README!!! 
by Hagar Yungman 204030712 and Noa Milshtein 303045454.

Project goal: analyze reviews in order to determine if a review is sarcastic or not.   
For that, the project uses a distributed system programming and Stanford NLP libraries.

Explanation: all AWS components are located on Virginia
AWS services used in the program:
* EC2 - manager with t2Micro @image, workers with t2Medium @image, the image is combined for both worker and manager 
* SQS - all queues are standard queues with long polling and visibility timeout of 2 to 4 minutes 
* S3 - bucket name: noahagarsarcasm-{app-UUID}/sarcasm
inside there are 2 folders: input where the input files will be upload to, and output where the manager will upload the result files. 
*IAM- create roles for the EC2 instances.

In the program, there are 3 queues named:
- "app-manager" (for communication between the application and the manager).
- "manager-workers" (for communication between the manager and all the workers).
- "workers-manager" (for communication between the workers and the manager).
for every application, we create a UUID that will identify this application in the program resulting in a unique S3 bucket and "manager-app-{UUID}" queue.

Application job:
* check if there is a queue named "app-manager" on AWS if not create it
* create a queue named "manager-app-{UUID}" 
* create a S3 bucket with input and output folders.
* setting terminate parameter 
* each input file is uploaded to the input folder in S3 and triggers the creation of a message to the "manager-app", queue.
* if the terminate parameter is set then terminate message send to "manager-app", queue.
* create or restart a manager type instance.
* the application will then starts an infinite loop in which it will check for messages in "manager-app-{UUID}" queue.
* each message is downloaded from output folder in S3 and parsed into an HTML file, located in dsp\application.
*delete the handheld message

Manager:
* the manager consist 2 threads one for handling the queue: "app-manager" and the other for handling "workers-manager".
* there is a shared object called ReviewsMap that holds:
- for each file, count of reviews per file.
- for each file, the URL of the application sending the file.
- an atomic boolean holds termination signal.
HandleAppMsg:  
in an infinite loop:
* get a message from "app-manager".
* parse the file in the message, line by line.
* create a message for every review in the line and upload it to "manager-workers".
* for every n messages create a worker.
* delete the handled message.
HandleWorkersMsg:
in an infinite loop:
* get a message from "workers-manager"- result from the workers.
* write the message, in the corresponding file saved in the computer.
* after all reviews regarding the same file are handled, upload the file to output folder and send a message to "manager-app-{UUID}".
* create a message regarding the location of the file and upload it to "manager-app-{UUID}"
* if the boolean terminate was set to true, then after uploading all the files, the workers are terminated, the "worker-manger" and "manager-workers" queues are deleted  
* delete the handled message.

Workers: 
in an infinite loop:
* get a message from "manager-workers"- review.
* analyze the review with Stanford NLP libraries.
* return the analyzed result to "workers-manager".
* delete the handled message.

How to run the project:
All input files should be placed in dsp\application. 
The order of the command arguments should be:  inputFileName1… inputFileNameN outputFileName1… outputFileNameN  n terminate, where n is workers to files ratio, and terminate is optional. 
@run
@Time to run, which n we used.

Did you think for more than 2 minutes about security? Do not send your credentials in plain text!
-Yes, we don't give our credentials when we create an EC2 instance, we use IAM role written by us for the specific use of the instance.

Did you think about scalability? Will your program work properly when 1 million clients connected at the same time? How about 2 million? 1 billion? Scalability is very important aspect of the system, be sure it is scalable!
-the workers work on one review at a time, so scalability is not going to be a problem for them. 
S3, SQS supports a huge amount of data. the manager can run with 2 threads and every thread performs a simple task on a single message, so it not affected by the size of the SQS queues in terms of scalability. the parameter n should be proportionate to the number of reviews, so all the reviews will be calculated faster.

What about persistence? What if a node dies? What if a node stalls for a while? Have you taken care of all possible outcomes in the system? Think of more possible issues that might arise from failures. What did you do to solve it? What about broken communications? Be sure to handle all fail-cases!
-for the situation that a node dies or stalls or the communication fails, we added a visibility time in all SQS, so if something happens, the message won't get lost.

Threads in your application, when is it a good idea? When is it bad? Invest time to think about threads in your application!
-we have 2 different threads in the manager that performs an infinite loop on different SQS, it a good idea because 1 thread can't be in 2 different infinite loops at the same time.
it not a good idea a good idea when threads need to access the same property.

Did you run more than one client at the same time? Be sure they work properly, and finish properly, and your results are correct.
-yes.

Do you understand how the system works? Do a full run using pen and paper, draw the different parts and the communication that happens between them.
-yes, we do. see the explanation above.

Did you manage the termination process? Be sure all is closed once requested!
-yes, see the explanation above.

Did you take in mind the system limitations that we are using? Be sure to use it to its fullest!
- yes, we selected standard SQS because it has no size limitation, so is S3. 
the manager performs a simple task so its a micro server and the workers perform a more complicated task, therefore, they need more memory, so they are set to be medium servers.

Are all your workers working hard? Or some are slacking? Why?
-there are shorter reviews and longer reviews so naturally, the work doesn't split evenly.

Is your manager doing more work than he's supposed to? Have you made sure each part of your system has properly defined tasks? Did you mix their tasks? Don't!
- yes, every component has a defined job.

Lastly, are you sure you understand what distributed means? Is there anything in your system awaiting another?
-yes, there for instead of creating a multi-thread system for the workers, we use multiple instances to preform the task.



RUN STATISTICS
we run the program with all 5 input file, n=250 and terminate. it took about 6 minutes to finish
number of workers: 12
number of reviews each worker handled: 209, 198, 121, 145, 16, 140, 176, 158, 154, 146, 198, 233 
number of messages read from app by manager - 5 (without terminate)
number of reviews handled by manager: 1896


requirements:
to run the program use the code:
java -jar mainproject.jar <input files> <output files> n <terminate - optional>
in the jar location you need to put the input files, noa.txt, AssumeEc2Role.json, ManagerPolicy.json, WorkerPolicy.json.







