package com.sarcasm;

import com.amazonaws.services.sqs.model.Message;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.rnn.RNNCoreAnnotations;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.CoreMap;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;

public class OutputReview {
    private final static HashSet<String> INTERESTING_ENTITIES = new HashSet<>();
    private static StanfordCoreNLP NERPipeline;
    private static StanfordCoreNLP sentimentPipeline;

    private InputReview review;
    private int sentiment;
    private boolean isSarcasm;
    private List<String> entities = new ArrayList<>();

    public static void setup(){
        INTERESTING_ENTITIES.add("PERSON");
        INTERESTING_ENTITIES.add("LOCATION");
        INTERESTING_ENTITIES.add("ORGANIZATION");

        //set sentiment properties
        Properties props = new Properties();
        props.put("annotators", "tokenize, ssplit, parse, sentiment");
        sentimentPipeline = new StanfordCoreNLP(props);
        //set entities properties
        props = new Properties();
        props.put("annotators", "tokenize , ssplit, pos, lemma, ner");
        NERPipeline = new StanfordCoreNLP(props);
    }

    public OutputReview(Message message){
        ObjectMapper mapper = new ObjectMapper();
        try {
            review = mapper.readValue(message.getBody(), InputReview.class);
            System.out.println("got new message id: " + review.getId() + "from file: "+ review.getFileName());

        } catch (IOException e) {
            e.printStackTrace();
            // should never happen
        }

        //add the review title to the review
        String reviewText = review.getTitle() + ". " + review.getText();
        //send review to get the sentiment
        setSentiment(reviewText);
        System.out.println("set sentiment to message");
        //if the sentimet != to the rating then the review is sarcastic
        //TODO calculate with fraction
        isSarcasm = Math.abs(sentiment - review.getRating()) > 2;
        //send review to get the Entities
        setEntities(reviewText);
        System.out.println("set entities to message");
    }

    @Override
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            // should never happen
            return null;
        }
    }

    public InputReview getReview() {
        return review;
    }

    public String getColorSentiment() {
        switch (sentiment){
            case 0:
                return "DarkRed";
            case 1:
                return "Red";
            case 2:
                return "Black";
            case 3:
                return "LightGreen";
            case 4:
                return "DarkGreen";
            default:
                return null;
        }
    }

    public int getSentiment() {
        return sentiment;
    }
    /*
        code performs sentiment analysis on a review
     */
    private void setSentiment(String review) {
        if (review != null && review.length() > 0) {
            int longest = 0;
            Annotation annotation = sentimentPipeline.process(review);
            for (CoreMap sentence : annotation
                    .get(CoreAnnotations.SentencesAnnotation.class)) {
                Tree tree = sentence
                        .get(SentimentCoreAnnotations.AnnotatedTree.class);
                int currentSentiment = RNNCoreAnnotations.getPredictedClass(tree);
                String partText = sentence.toString();
                if (partText.length() > longest) {
                    sentiment = currentSentiment;
                    longest = partText.length();
                }

            }
        }
    }

    public boolean isSarcasm() {
        return isSarcasm;
    }

    public List<String> getEntities() {
        return entities;
    }

    /*
        code extracts named entities from a review
        Extract only the following entity types: PERSON, LOCATION, ORGANIZATION. Initialization:
     */
    private void setEntities(String review) {
        // create an empty Annotation just with the given text
        Annotation document = new Annotation(review);

        // run all Annotators on this text
        NERPipeline.annotate(document);

        // these are all the sentences in this document
        // a CoreMap is essentially a Map that uses class objects as keys and has values with custom types
        List<CoreMap> sentences = document.get(CoreAnnotations.SentencesAnnotation.class);
        for (CoreMap sentence : sentences) {
            // traversing the words in the current sentence
            // a CoreLabel is a CoreMap with additional token-specific methods
            for (CoreLabel token : sentence.get(CoreAnnotations.TokensAnnotation.class)) {
                // this is the text of the token
                String word = token.get(CoreAnnotations.TextAnnotation.class);
                // this is the NER label of the token
                String ne = token.get(CoreAnnotations.NamedEntityTagAnnotation.class);
                if (INTERESTING_ENTITIES.contains(ne)) {
                    entities.add(word + ":" + ne);
                }
            }
        }
    }
}
