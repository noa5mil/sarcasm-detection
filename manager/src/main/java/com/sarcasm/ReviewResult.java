package com.sarcasm;

import java.util.List;

public class ReviewResult {
    private Review review;
    private String colorSentiment;
    private int sentiment;
    private boolean isSarcasm;
    private List<String> entities;

    public Review getReview() {
        return review;
    }

    public void setReview(Review review) {
        this.review = review;
    }

    public String getColorSentiment() {
        return colorSentiment;
    }

    public void setColorSentiment(String colorSentiment) {
        this.colorSentiment = colorSentiment;
    }

    public int getSentiment() {
        return sentiment;
    }

    public void setSentiment(int sentiment) {
        this.sentiment = sentiment;
    }

    public boolean isSarcasm() {
        return isSarcasm;
    }

    public void setSarcasm(boolean sarcasm) {
        isSarcasm = sarcasm;
    }

    public List<String> getEntities() {
        return entities;
    }

    public void setEntities(List<String> entities) {
        this.entities = entities;
    }
}
