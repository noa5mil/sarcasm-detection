package com.sarcasm;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

public class ReviewsMap {
    private ConcurrentHashMap<String, Integer> numOfReviewsPerFile;
    private ConcurrentHashMap<String, String> filesUrls;
    public final AtomicBoolean terminate = new AtomicBoolean(false);

    public ReviewsMap(){
           this.numOfReviewsPerFile = new ConcurrentHashMap<String, Integer>();
           this.filesUrls = new ConcurrentHashMap<String,String>();
    }

        public ConcurrentHashMap<String, Integer> getNumOfReviewsPerFile() {
            return numOfReviewsPerFile;
        }

    public ConcurrentHashMap<String, String> getFilesUrls() {
        return filesUrls;
    }

}
