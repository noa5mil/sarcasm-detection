package com.sarcasm;

public class MsgToApp {
    private String fileName;
    private String bucketName;
    private String outputKeyName;

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getOutputKeyName() {
        return outputKeyName;
    }

    public void setOutputKeyName(String outputKeyName) {
        this.outputKeyName = outputKeyName;
    }

    public String getFileName() {

        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
