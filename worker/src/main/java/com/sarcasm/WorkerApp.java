package com.sarcasm;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;

import java.util.List;

public class WorkerApp {

    private final static String MANAGER_WORKERS_QUEUE_NAME = "manager-workers";
    private final static String WORKERS_MANAGER_QUEUE_NAME = "workers-manager";
    public final static Regions REGION = Regions.US_EAST_1;

    private static AmazonSQS SQS = AmazonSQSClientBuilder.standard()
            .withRegion(REGION)
            .build();
    private final static String MANAGER_WORKERS_QUEUE_URL = getSQSUrl(MANAGER_WORKERS_QUEUE_NAME);
    private final static String WORKERS_MANAGER_QUEUE_URL = getSQSUrl(WORKERS_MANAGER_QUEUE_NAME);
    private static int reviewsRead = 0;

    public static void main(String[] args) {
        OutputReview.setup();

        //get messages from manager
        while (true) {
            ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(MANAGER_WORKERS_QUEUE_NAME);
            List<Message> messages = SQS.receiveMessage(receiveMessageRequest).getMessages();
            for (Message message : messages) {
                reviewsRead += 1;
                //parse message to review
                OutputReview result = new OutputReview(message);
                // send result to workers-manager queue
                SQS.sendMessage(new SendMessageRequest()
                        .withQueueUrl(WORKERS_MANAGER_QUEUE_URL)
                        .withMessageBody(result.toString()));
                System.out.println("send message to workers-manager queue");
                //delete message from queue
                SQS.deleteMessage(MANAGER_WORKERS_QUEUE_URL, message.getReceiptHandle());
                System.out.println("deleted the message");
                System.out.println("reviews read - " + reviewsRead);
            }
        }
    }

    /*
            get sqs url according to the parameter sqsNAme
     */
    private static String getSQSUrl(String sqsName) {
        try {
            String result = SQS.getQueueUrl(sqsName).getQueueUrl();
            System.out.println("get sqs: "+ sqsName + "url -------------------------------" +result);
            return result;
        }catch (com.amazonaws.services.sqs.model.QueueDoesNotExistException e) {
            System.out.println("queue was not found");
            return null;
        }
    }
}
